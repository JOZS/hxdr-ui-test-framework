package com.hexagon.hxdr.testing.pages.project;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import lombok.Getter;
import org.openqa.selenium.WebDriver;

@Getter
public class ProjectPage extends AbstractPage {

    private ProjectNavigationPanel projectNavigationPanel;

    public ProjectPage(WebDriver driver) {
        super(driver);
        projectNavigationPanel = new ProjectNavigationPanel(driver);
    }
}
