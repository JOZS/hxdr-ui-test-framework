package com.hexagon.hxdr.testing.pages.project;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Getter
@Slf4j
public class MyProjectsPage extends AbstractPage {

    @FindBy(css = "[data-test-id='open-project-modal-button']")
    private WebElement createProjectButton;
    @FindBy(css = "[data-test-id='project-list-entries'] > div")
    private List<WebElement> projectContainerWebElements;

    private final List<ProjectContainer> projectContainers = new ArrayList<>();

    public MyProjectsPage(WebDriver driver) {
        super(driver);
        log.info("Collecting project containers in My Projects Page...");
        projectContainerWebElements.forEach(p -> projectContainers.add(new ProjectContainer(p, driver)));
        log.info("{} project container(s) found ...", projectContainers.size());
    }

    public MyProjectsPage createProject(String title, String description, Path coverImage) {
        log.info("Creating new project ... ");
        wait.until(ExpectedConditions.elementToBeClickable(createProjectButton)).click();
        return new CreateProjectDialog(driver)
                .fillTitle(title)
                .fillDescription(description)
                .addCoverImage(coverImage)
                .clickToCreateProject();
    }

    public ProjectPage openProject(String projectTitle) {
        log.info("Opening project: {} ...", projectTitle);
        driver.findElement(By.xpath("//h3[contains(text(), '" + projectTitle + "')]")).click();
        return new ProjectPage(driver);
    }

    public MyProjectsPage deleteProject(String projectTitle) {
        log.info("Deleting project {} ...", projectTitle);
        projectContainers.stream()
                .filter(pc -> pc.getProjectTitle().equals(projectTitle))
                .findFirst()
                .ifPresentOrElse(ProjectContainer::delete, () -> {
                    throw new IllegalStateException("Project couldn't be find for deletion: " + projectTitle);
                });
        return this;
    }
}
