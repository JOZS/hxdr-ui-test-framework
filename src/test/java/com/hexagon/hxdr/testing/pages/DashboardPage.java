package com.hexagon.hxdr.testing.pages;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Slf4j
public class DashboardPage extends AbstractPage {

    @FindBy(css = "button[data-test-id='shop-hexagon-data']")
    private WebElement openCatalogButton;

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    public CatalogPage openCatalog() {
        log.info("Opening catalog...");
        openCatalogButton.click();
        return new CatalogPage(driver);
    }
}
