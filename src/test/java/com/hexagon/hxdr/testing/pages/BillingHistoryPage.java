package com.hexagon.hxdr.testing.pages;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

@Getter
@Slf4j
public class BillingHistoryPage extends AbstractPage {

    @FindBy(css = "[data-test-id^='list-view-row']")
    private List<WebElement> orderRowWebElements;

    private final List<Order> orders = new ArrayList<>();

    public BillingHistoryPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-test-id='list-view-items']")));
        log.info("Collecting orders...");
        orderRowWebElements.forEach(r -> orders.add(new Order(r)));
        log.info("{} order(s) found ...", orderRowWebElements.size());
    }
}
