package com.hexagon.hxdr.testing.pages.project;

import com.hexagon.hxdr.testing.pages.project.tabs.ProjectAssetsTab;
import com.hexagon.hxdr.testing.pages.project.tabs.ProjectDetailsTab;
import com.hexagon.hxdr.testing.pages.project.tabs.ProjectTeamTab;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Slf4j
public class ProjectNavigationPanel {

    @FindBy(css = "[data-test-id='assets-tab']")
    private WebElement assetsTab;
    @FindBy(css = "[data-test-id='details-tab']")
    private WebElement detailsTab;
    @FindBy(css = "[data-test-id='team-tab']")
    private WebElement teamTab;

    private final WebDriver driver;

    public ProjectNavigationPanel(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public ProjectAssetsTab openProjectAssetsTab(){
        log.info("Opening project assets tab ...");
        assetsTab.click();
        return new ProjectAssetsTab(driver);
    }

    public ProjectDetailsTab openDetailsTab(){
        log.info("Opening project details tab ...");
        detailsTab.click();
        return new ProjectDetailsTab(driver);
    }

    public ProjectTeamTab openTeamTab(){
        log.info("Opening project team tab ...");
        teamTab.click();
        return new ProjectTeamTab(driver);
    }
}
