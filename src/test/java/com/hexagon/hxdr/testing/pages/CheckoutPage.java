package com.hexagon.hxdr.testing.pages;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;

@Slf4j
public class CheckoutPage extends AbstractPage {

    @FindBy(css = "iframe[title='Secure card number input frame']")
    private WebElement creditCardInputIFrame;
    @FindBy(css = "iframe[title='Secure expiration date input frame']")
    private WebElement expirationDateInputIFrame;
    @FindBy(css = "iframe[title='Secure CVC input frame']")
    private WebElement cvcInputIFrame;
    @FindBy(name = "cardnumber")
    private WebElement cardNumberInput;
    @FindBy(name = "exp-date")
    private WebElement expirationDateInput;
    @FindBy(name = "cvc")
    private WebElement cvcInput;
    @FindBy(xpath = "//input[@data-test-id='accept-agreement-checkbox']/..")
    private WebElement acceptAgreementCheckbox;
    @FindBy(css = "[data-test-id='pay-button']")
    private WebElement payButton;
    @FindBy(css = "[data-test-id='purchase-button']")
    private WebElement purchaseButton;
    @FindBy(xpath = "//div[contains(text(), 'Go to billing history')]/..")
    private WebElement goToBillingHistoryButton;
    @FindBy(css = "[data-test-id='checkout-amount']")
    private WebElement subTotalField;
    @FindBy(css = "[data-test-id='discount-value']")
    private WebElement discountedPriceField;
    @FindBy(xpath = "//div[contains(text(), 'Total')]/following-sibling::div")
    private WebElement totalPriceField;
    @FindBys({
            @FindBy(xpath = "//div[contains(text(), 'VAT')]/following-sibling::div"),
            @FindBy(xpath = "//div[contains(text(), 'TAX')]/following-sibling::div")
    }
    )
    private WebElement taxField;


    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    public CheckoutPage fillCardNumber(String cardNumber) {
        log.info("Filling card number with: {} ...", cardNumber);
        driver.switchTo().defaultContent();
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(creditCardInputIFrame));
        cardNumberInput.sendKeys(cardNumber);
        driver.switchTo().defaultContent();
        return this;
    }

    public CheckoutPage fillExpirationDate(String expirationDate) {
        log.info("Filling expiration date with: {} ...", expirationDate);
        driver.switchTo().defaultContent();
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(expirationDateInputIFrame));
        expirationDateInput.sendKeys(expirationDate);
        driver.switchTo().defaultContent();
        return this;
    }

    public CheckoutPage fillCVC(String CVC) {
        log.info("Filling CVC with: {} ...", CVC);
        driver.switchTo().defaultContent();
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(cvcInputIFrame));
        cvcInput.sendKeys(CVC);
        driver.switchTo().defaultContent();
        return this;
    }

    public CheckoutPage acceptOrderAgreement() {
        log.info("Accepting order agreement...");
        acceptAgreementCheckbox.click();
        return this;
    }

    public CheckoutPage clickToPayButton() {
        log.info("Clicking to the Pay button ...");
        payButton.click();
        return this;
    }

    public CheckoutPage clickToPurchaseButton() {
        log.info("Clicking to the Purchase button ...");
        purchaseButton.click();
        return this;
    }

    public BillingHistoryPage goToBillingHistory() {
        log.info("Going to billing history page ...");
        wait.until(ExpectedConditions.elementToBeClickable(goToBillingHistoryButton)).click();
        return new BillingHistoryPage(driver);
    }

    public double getSubtotal() {
        log.info("Parsing subtotal...");
        double subTotal = Double.parseDouble(subTotalField.getText());
        log.info("Subtotal: {}", subTotal);
        return subTotal;
    }

    public double getDiscountedPrice() {
        log.info("Parsing discounted price ...");
        double discountedPrice = Double.parseDouble(discountedPriceField.getText());
        log.info("Discounted price: {}", discountedPrice);
        return discountedPrice;
    }

    public double getTax() {
        log.info("Parsing tax value ...");
        double tax = Double.parseDouble(taxField.getText());
        log.info("Tax: {} %", tax);
        return tax;
    }

    public double getTotalPrice() {
        log.info("Parsing total price ...");
        double totalPrice = Double.parseDouble(totalPriceField.getText()
                .replaceAll(" ", "")
                .replaceAll(",", "")
                .replaceAll("EUR", "")
                .replaceAll("USD", "")
                .replaceAll("CHF", ""));
        log.info("Total price: {}", totalPrice);
        return totalPrice;
    }
}
