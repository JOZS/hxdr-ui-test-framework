package com.hexagon.hxdr.testing.pages.project.tabs;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import com.hexagon.hxdr.testing.pages.project.MyProjectsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AbstractTab extends AbstractPage {

    @FindBy(css = "[data-test-id='back-to-projects']")
    private WebElement goBackToMyProjectsButton;

    public AbstractTab(WebDriver driver) {
        super(driver);
    }

    public MyProjectsPage goBackToMyProjects(){
        goBackToMyProjectsButton.click();
        return new MyProjectsPage(driver);
    }
}
