package com.hexagon.hxdr.testing.pages.project;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.nio.file.Path;

@Slf4j
public class CreateProjectDialog extends AbstractPage {

    @FindBy(id = "title")
    private WebElement projectTitleField;
    @FindBy(css = "[data-test-id='create-project-modal-description'] textarea")
    private WebElement descriptionField;
    @FindBy(css = "[data-test-id='cover-image-tab-modal']")
    private WebElement coverImageTab;
    @FindBy(css = "input[type='file']")
    private WebElement imageUploadInput;
    @FindBy(css = "[data-test-id='form-modal-submit-button']")
    private WebElement createProjectButton;
    @FindBy(css = "[data-test-id='form-modal-cancel-button']")
    private WebElement cancelButton;

    public CreateProjectDialog(WebDriver driver) {
        super(driver);
    }

    public CreateProjectDialog fillTitle(String title) {
        log.info("Filling title with: {} ...", title);
        projectTitleField.sendKeys(title);
        return this;
    }

    public CreateProjectDialog fillDescription(String description) {
        log.info("Filling description with: {} ...", description);
        descriptionField.sendKeys(description);
        return this;
    }

    public CreateProjectDialog addCoverImage(Path coverImage) {
        log.info("Uploading cover image: {} ...", coverImage.toAbsolutePath());
        coverImageTab.click();
        imageUploadInput.sendKeys(coverImage.toAbsolutePath().toString());
        return this;
    }

    public MyProjectsPage clickToCreateProject() {
        wait.until(ExpectedConditions.elementToBeClickable(createProjectButton)).click();
        return new MyProjectsPage(driver);
    }
}
