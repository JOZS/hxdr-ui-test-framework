package com.hexagon.hxdr.testing.pages;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class Order {

    private final WebElement orderRowWebElement;

    public Order(WebElement orderRowWebElement) {
        PageFactory.initElements(orderRowWebElement, this);
        this.orderRowWebElement = orderRowWebElement;
    }

    @FindBy(css = "[data-test-id$='-orderid']")
    private WebElement orderId;
    @FindBy(css = "[data-test-id$='-purchaseDate']")
    private WebElement purchaseDate;
    @FindBy(css = "[data-test-id$='-price']")
    private WebElement price;
    @FindBy(css = "div:nth-of-type(3)")
    private WebElement status;

    public String getOrderId() {
        return orderId.getText();
    }

    public String getPurchaseDate() {
        return purchaseDate.getText();
    }

    public String getPrice() {
        return price.getText();
    }

    public String getStatus() {
        return status.getText();
    }
}
