package com.hexagon.hxdr.testing.pages.project.tabs;

import com.hexagon.hxdr.testing.pages.project.AssetViewerPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.nio.file.Path;

public class ProjectAssetsTab extends AbstractTab {

    @FindBy(css = "[data-test-id='global-upload']")
    private WebElement assetUploadInput;

    public ProjectAssetsTab(WebDriver driver) {
        super(driver);
    }

    public ProjectAssetsTab uploadAsset(Path asset) {
        assetUploadInput.sendKeys(asset.toAbsolutePath().toString());
        return this;
    }

    public AssetViewerPage openAsset(String assetName) {
        driver.findElement(By.xpath("//p[ contains (text(), '" + assetName + "' ) ]")).click();
        return new AssetViewerPage(driver);
    }
}
