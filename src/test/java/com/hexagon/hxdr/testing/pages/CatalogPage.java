package com.hexagon.hxdr.testing.pages;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Slf4j
public class CatalogPage extends AbstractPage {

    @FindBy(name = "search")
    private WebElement searchField;
    @FindBy(css = "span[data-test-id='area-selection']")
    private WebElement areaSelectionButton;
    @FindBy(css = "div[data-test-id=area-selection-mode-toggle]")
    private WebElement viewPortButton;
    @FindBy(css = "button[data-test-id=capture-button]")
    private WebElement captureViewButton;
    @FindBy(css = "button[data-test-id=addtocart]")
    private WebElement addToCartButton;
    @FindBy(css = "button[data-test-id=checkout]")
    private WebElement checkoutButton;
    @FindBy(css = "button[data-test-id=zoom-in]")
    private WebElement zoomInButton;

    public CatalogPage(WebDriver driver) {
        super(driver);
    }

    public CatalogPage search(String searchTerm) {
        var catalogPage = searchWithRetry(searchTerm, 5);
        sleep(TimeUnit.SECONDS, 10);
        return catalogPage;
    }

    public CatalogPage openAreaSelectionPanel() {
        log.info("Opening area selection menu ...");
        wait.until(ExpectedConditions.elementToBeClickable(areaSelectionButton)).click();
        return this;
    }

    public CatalogPage selectViewPort() {
        log.info("Selecting viewport ...");
        wait.until(ExpectedConditions.elementToBeClickable(viewPortButton)).click();
        return this;
    }

    public CatalogPage captureView() {
        log.info("Capturing view ...");
        wait.until(ExpectedConditions.elementToBeClickable(captureViewButton)).click();
        return this;
    }

    public CatalogPage addQuotationToCart() {
        log.info("Adding quotation to cart ...");
        wait.until(ExpectedConditions.elementToBeClickable(addToCartButton)).click();
        return this;
    }

    public CheckoutPage checkout() {
        log.info("Checking out ...");
        wait.until(ExpectedConditions.elementToBeClickable(checkoutButton)).click();
        return new CheckoutPage(driver);
    }

    private CatalogPage searchWithRetry(String searchTerm, int numberOfRetries) {
        try {
            log.info("Searching for {} ...", searchTerm);
            wait.until(ExpectedConditions.elementToBeClickable(searchField)).sendKeys(searchTerm);
            selectOptionFromSuggestions(searchTerm);
        } catch (Exception e) {
            if (numberOfRetries > 0) {
                log.warn("Couldn't execute search, retrying max {} more times ...", numberOfRetries);
                searchField.clear();
                searchWithRetry(searchTerm, --numberOfRetries);
            } else {
                throw new IllegalStateException(e);
            }
        }
        return this;
    }

    private void selectOptionFromSuggestions(String option) {
        sleep(TimeUnit.SECONDS, 3);
        driver.findElement(By.xpath("//span[ contains (text(), '" + option + "' ) ]")).click();
    }

    public CatalogPage zoomIn(int times) {
        log.info("Zooming in {} times...", times);
        IntStream.rangeClosed(1, times).forEach(x -> {
            log.info("Zooming in ...");
            zoomInButton.click();
            sleep(TimeUnit.SECONDS, 3);
        });
        return this;
    }
}
