package com.hexagon.hxdr.testing.pages.common;

import com.hexagon.hxdr.testing.pages.common.SideMenuPanel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Data
@Slf4j
public abstract class AbstractPage {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected SideMenuPanel sideMenuPanel;
    protected Actions action;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(60));
        sideMenuPanel = new SideMenuPanel(driver);
        action = new Actions(driver);
        PageFactory.initElements(this.driver, this);
    }

    protected void sleep(TimeUnit timeUnit, long timeOut) {
        try {
            log.info("Sleeping {} {} ...", timeOut, timeUnit.name());
            TimeUnit.valueOf(timeUnit.name()).sleep(timeOut);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void scrollElementToView(WebElement webElement) {
        log.info("Scrolling element to view ...");
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded()", webElement);
        sleep(TimeUnit.MILLISECONDS, 500);
    }

}
