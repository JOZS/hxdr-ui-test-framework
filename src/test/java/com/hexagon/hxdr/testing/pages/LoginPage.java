package com.hexagon.hxdr.testing.pages;

import com.hexagon.hxdr.testing.config.model.TestUser;
import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Slf4j
public class LoginPage extends AbstractPage {

    private final String baseURL;

    @FindBy(xpath = "//button[ contains (text(), 'Accept All Cookies' ) ]")
    private WebElement acceptCookiesButton;
    @FindBy(id = "email")
    private WebElement emailInput;
    @FindBy(id = "password")
    private WebElement passwordInput;
    @FindBy(css = "button[data-test-id='login-submit']")
    private WebElement loginButton;

    public LoginPage(WebDriver driver, String baseURL) {
        super(driver);
        this.baseURL = baseURL;
    }

    public LoginPage open() {
        String loginURL = baseURL + "/login";
        log.info("Navigating to {} ...", loginURL);
        driver.navigate().to(loginURL);
        return this;
    }

    public LoginPage acceptAllCookies() {
        log.info("Accepting cookies...");
        this.acceptCookiesButton.click();
        return this;
    }

    public DashboardPage login(TestUser testUser) {
        return login(testUser.getEmail(), testUser.getPassword());
    }

    public DashboardPage login(String email, String password) {
        log.info("Login with user: {} ...", email);
        this.emailInput.sendKeys(email);
        this.passwordInput.sendKeys(password);
        loginButton.click();
        return new DashboardPage(driver);
    }
}
