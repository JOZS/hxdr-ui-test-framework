package com.hexagon.hxdr.testing.pages.common;

import com.hexagon.hxdr.testing.pages.project.MyProjectsPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

@Slf4j
public class SideMenuPanel {
    @FindBy(css = "button[data-test-id='side-menu-icon']")
    private WebElement sideMenuPanelButton;
    @FindBy(linkText = "My Projects")
    private WebElement myProjectsMenu;
    @FindBy(linkText = "Dashboard")
    private WebElement dashboardMenu;

    private final WebDriver driver;

    public SideMenuPanel(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SideMenuPanel open() {
        log.info("Opening side menu panel ...");
        var wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.elementToBeClickable(sideMenuPanelButton)).click();
        log.info("Side menu panel is open.");
        return this;
    }

    public MyProjectsPage openMyProjects() {
        log.info("Opening My Projects page ...");
        var wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.elementToBeClickable(myProjectsMenu)).click();
        return new MyProjectsPage(driver);
    }
}
