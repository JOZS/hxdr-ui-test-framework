package com.hexagon.hxdr.testing.pages.project;

import com.hexagon.hxdr.testing.pages.common.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProjectContainer extends AbstractPage {

    private final WebElement containerWebElement;

    public ProjectContainer(WebElement containerWebElement, WebDriver driver) {
        super(driver);
        PageFactory.initElements(containerWebElement, this);
        this.containerWebElement = containerWebElement;
    }

    @FindBy(css = "[data-test-id*='project-title']")
    private WebElement projectTitle;
    @FindBy(css = "[data-test-id*='project-number-of-assets']")
    private WebElement numberOfAssets;
    @FindBy(css = "[data-test-id='project-actions-button']")
    private WebElement projectActionsButton;
    @FindBy(css = "[data-test-id*='delete-project-menu-item']")
    private WebElement deleteProjectMenuItem;

    public String getProjectTitle() {
        return projectTitle.getText();
    }

    public int getNumberOfAssets() {
        return Integer.parseInt(numberOfAssets.getText());
    }

    public void delete() {
        scrollElementToView(projectTitle);
        action.moveToElement(projectTitle).perform();
        projectActionsButton.click();
        deleteProjectMenuItem.click();
        driver.findElement(By.cssSelector("[data-test-id='remove-member-button']")).click();
    }

}
