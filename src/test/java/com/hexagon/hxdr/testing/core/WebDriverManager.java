package com.hexagon.hxdr.testing.core;

import org.openqa.selenium.WebDriver;

public interface WebDriverManager {
    void createNewDriver();

    void closeDriver();

    WebDriver getDriver();
}
