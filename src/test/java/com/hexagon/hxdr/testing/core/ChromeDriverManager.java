package com.hexagon.hxdr.testing.core;

import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.WebDriverListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.time.Duration;

@Service
@Setter
@Getter
@Qualifier("driverManager")
public class ChromeDriverManager implements WebDriverManager {

    private WebDriver driver;

    @PostConstruct
    public void setUpSeleniumPath() {
        var chromeDriverPath = Paths.get("src/test/resources/chromedriver").toAbsolutePath();
        System.setProperty("webdriver.chrome.driver", chromeDriverPath.toString());
    }

    public void createNewDriver() {
        if (driver != null) {
            this.closeDriver();
        }
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("ignore-gpu-blocklist");
        chromeOptions.addArguments("enable-features=VaapiVideoDecoder");
        var driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        driver.manage().timeouts().scriptTimeout(Duration.ofSeconds(15));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(15));
        this.driver = getEventListenerDecoratedDriver(driver);
    }

    public void closeDriver() {
        driver.close();
        driver.quit();
        driver = null;
    }

    public WebDriver getDriver() {
        if (driver == null) {
            this.createNewDriver();
        }
        return driver;
    }

    private WebDriver getEventListenerDecoratedDriver(WebDriver driver) {
        WebDriverListener listener = new WebDriverListener() {
            @Override
            public void onError(Object target, Method method, Object[] args, InvocationTargetException e) {
                System.out.println();
            }
        };
        return new EventFiringDecorator(listener).decorate(driver);
    }
}
