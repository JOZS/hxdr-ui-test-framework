package com.hexagon.hxdr.testing.core.extensions;


import com.hexagon.hxdr.testing.core.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Slf4j
public class AfterEachExtension implements TestWatcher, AfterEachCallback {

    private final WebDriverManager webDriverManager;

    public AfterEachExtension(WebDriverManager webDriverManager) {
        this.webDriverManager = webDriverManager;
    }

    public void testFailed(ExtensionContext context, Throwable cause) {
        createScreenshot(context);
        webDriverManager.closeDriver();
    }

    public void testSuccessful(ExtensionContext context) {
        webDriverManager.closeDriver();
    }

    @Override
    public void afterEach(ExtensionContext extensionContext) {
        log.info("Execution of test case <{}> has finished.", extensionContext.getTestMethod().get().getName());
    }

    private void createScreenshot(ExtensionContext context) {
        TakesScreenshot screenshot = ((TakesScreenshot) webDriverManager.getDriver());
        File screenshotFile = screenshot.getScreenshotAs(OutputType.FILE);
        Path screenshotPath = Path.of("./target/screenshots");
        try {
            Files.createDirectories(screenshotPath);
            Files.copy(screenshotFile.toPath(),
                    screenshotPath.resolve(context.getTestMethod().get().getName()
                            + "_"
                            + System.currentTimeMillis()
                            + ".png"),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
