package com.hexagon.hxdr.testing.core.extensions;

import com.hexagon.hxdr.testing.core.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

@Slf4j
public class BeforeEachExtension implements TestWatcher, BeforeEachCallback {

    private final WebDriverManager webDriverManager;

    public BeforeEachExtension(WebDriverManager webDriverManager) {
        this.webDriverManager = webDriverManager;
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext) {
        log.info("Starting test case <{}> ...", extensionContext.getTestMethod().get());
        webDriverManager.createNewDriver();
    }
}
