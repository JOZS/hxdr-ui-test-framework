package com.hexagon.hxdr.testing.tests;

import com.hexagon.hxdr.testing.config.model.TestUser;
import com.hexagon.hxdr.testing.config.model.enums.UserKey;
import com.hexagon.hxdr.testing.pages.CheckoutPage;
import com.hexagon.hxdr.testing.pages.LoginPage;
import com.hexagon.hxdr.testing.pages.Order;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PurchaseTests extends AbstractTest {

    @ParameterizedTest
    @CsvSource(value = {"COMPANY_A_ADMIN:Washington, DC, United States", "COMPANY_A_MAINTAINER:Grande Prairie, AB, Canada"}, delimiter = ':')
    public void testPurchaseWithCreditCard(UserKey userKey, String searchTerm) {
        CheckoutPage checkoutPage = new LoginPage(webDriverManager.getDriver(), environmentProperties.getUrl())
                .open()
                .acceptAllCookies()
                .login(getUser(userKey))
                .openCatalog()
                .search(searchTerm)
                .zoomIn(2)
                .openAreaSelectionPanel()
                .selectViewPort()
                .captureView()
                .addQuotationToCart()
                .checkout();

        double checkoutPageTotalPrice = checkoutPage.getTotalPrice();
        assertThat(checkoutPageTotalPrice)
                .withFailMessage("Too big total price: %s", checkoutPageTotalPrice)
                .isLessThan(500);

        Order lastOrder = checkoutPage.fillCardNumber("4111 1111 1111 1111")
                .fillExpirationDate("04 / 28")
                .fillCVC("123")
                .acceptOrderAgreement()
                .clickToPayButton()
                .goToBillingHistory()
                .getOrders()
                .get(0);

        assertThat(lastOrder.getOrderId()).matches("^[0-9]{4}-[0-9]{6}$");
        assertThat(lastOrder.getPurchaseDate()).isNotEmpty();
        assertThat(lastOrder.getPrice()).isNotEmpty();
        assertThat(lastOrder.getStatus()).isIn("Pending", "Processed");
    }

    @ParameterizedTest
    @EnumSource(value = UserKey.class, names = {"RESELLER_A_ADMIN", "RESELLER_B_ADMIN"})
    public void testPurchaseWithInvoice(UserKey userKey) {
        TestUser user = getUser(userKey);
        CheckoutPage checkoutPage = new LoginPage(webDriverManager.getDriver(), environmentProperties.getUrl())
                .open()
                .acceptAllCookies()
                .login(user)
                .openCatalog()
                .search("Washington, DC, United States")
                .openAreaSelectionPanel()
                .selectViewPort()
                .captureView()
                .addQuotationToCart()
                .checkout();

        double checkoutPageSubtotal = checkoutPage.getSubtotal();
        double checkoutPageDiscountedPrice = checkoutPage.getDiscountedPrice();
        double checkoutPageTotalPrice = checkoutPage.getTotalPrice();
        assertThat(checkoutPageSubtotal - checkoutPageSubtotal * user.getAccount().getDiscountRate()).isEqualTo(checkoutPageDiscountedPrice);
        assertThat(checkoutPageTotalPrice)
                .withFailMessage("Too big total price: %s", checkoutPageTotalPrice)
                .isLessThan(500);

        Order lastOrder = checkoutPage.acceptOrderAgreement()
                .clickToPurchaseButton()
                .goToBillingHistory()
                .getOrders()
                .get(0);

        assertThat(lastOrder.getOrderId()).matches("^[0-9]{4}-[0-9]{6}$");
        assertThat(lastOrder.getPurchaseDate()).isNotEmpty();
        assertThat(lastOrder.getPrice()).isNotEmpty();
        assertThat(lastOrder.getStatus()).isIn("Pending", "Processed");
    }
}
