package com.hexagon.hxdr.testing.tests;

import com.hexagon.hxdr.testing.config.model.enums.UserKey;
import com.hexagon.hxdr.testing.pages.LoginPage;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;

public class ProjectCentricTests extends AbstractTest{

    @Test
    public void createProjectAndUploadAssetTest(){
        String projectTitle = "Test_Project_" + System.currentTimeMillis();
        Path testAsset = Path.of("src/test/resources/testdata/0912101-01wall_layers_number_1.ifc");
        new LoginPage(webDriverManager.getDriver(), environmentProperties.getUrl())
                .open()
                .acceptAllCookies()
                .login(getUser(UserKey.COMPANY_A_ADMIN))
                .getSideMenuPanel()
                .open()
                .openMyProjects()
                .createProject(projectTitle,
                        "The description of my project",
                        Path.of("src/test/resources/testdata/GlobeImage.png"))
                .openProject(projectTitle)
                .getProjectNavigationPanel()
                .openProjectAssetsTab()
                .uploadAsset(testAsset)
                .goBackToMyProjects()
                .deleteProject(projectTitle);
    }

    @Test
    public void addProjectMembersWithDifferentRolesTest(){
        new LoginPage(webDriverManager.getDriver(), environmentProperties.getUrl())
                .open()
                .acceptAllCookies()
                .login(getUser(UserKey.COMPANY_A_ADMIN))
                .getSideMenuPanel()
                .open()
                .openMyProjects();
    }
}
