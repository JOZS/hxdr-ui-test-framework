package com.hexagon.hxdr.testing.tests;

import com.hexagon.hxdr.testing.config.Config;
import com.hexagon.hxdr.testing.config.EnvironmentProperties;
import com.hexagon.hxdr.testing.config.model.TestUser;
import com.hexagon.hxdr.testing.config.model.enums.UserKey;
import com.hexagon.hxdr.testing.core.WebDriverManager;
import com.hexagon.hxdr.testing.core.extensions.AfterEachExtension;
import com.hexagon.hxdr.testing.core.extensions.BeforeEachExtension;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.PostConstruct;
import java.time.Duration;

@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Config.class)
public abstract class AbstractTest {
    @Autowired
    protected WebDriverManager webDriverManager;
    @Autowired
    protected EnvironmentProperties environmentProperties;
    @RegisterExtension
    private BeforeEachExtension beforeEachExtension;
    @RegisterExtension
    private AfterEachExtension afterEachExtension;

    @PostConstruct
    private void postConstruct() {
        afterEachExtension = new AfterEachExtension(webDriverManager);
        beforeEachExtension = new BeforeEachExtension(webDriverManager);
    }

    protected WebDriverWait wait;

    protected TestUser getUser(UserKey userKey) {
        return environmentProperties.getUsers().get(userKey);
    }

    @BeforeEach
    public void init() {
        wait = new WebDriverWait(webDriverManager.getDriver(), Duration.ofSeconds(60));
    }
}
