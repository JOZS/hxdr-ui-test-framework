package com.hexagon.hxdr.testing.config;

import com.hexagon.hxdr.testing.config.model.Account;
import com.hexagon.hxdr.testing.config.model.TestUser;
import com.hexagon.hxdr.testing.config.model.enums.AccountKey;
import com.hexagon.hxdr.testing.config.model.enums.UserKey;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Map;

@Data
public class EnvironmentProperties {

    private String url;
    private String poolId;
    private String clientId;
    private String password;
    private String hxdrDistributionList;
    private String mailBucket;
    private String salesReportBucket;
    private String emailSuffix;
    private String backOfficeMail;

    private Map<AccountKey, Account> accounts;
    private Map<UserKey, TestUser> users;

    @PostConstruct
    private void mapUsersToAccounts() {
        users.values().forEach(u -> u.setAccount(accounts.get(u.getAccountKey())));
    }

    @PostConstruct
    private void setAccountAdmins() {
        users.forEach((key, value) -> {
            if (value.getAccountRole().equals("ACCOUNT_ROLE_ADMIN")) {
                accounts.get(value.getAccountKey()).setAdmin(key);
            }
        });
    }
}
