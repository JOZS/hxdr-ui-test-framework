package com.hexagon.hxdr.testing.config.model.enums;

public enum AccountKey {
    COMPANY_A,
    COMPANY_B,
    COMPANY_C,
    COMPANY_D,
    COMPANY_F,
    RESELLER_A,
    RESELLER_B,
    HXDR_ADMIN
}
