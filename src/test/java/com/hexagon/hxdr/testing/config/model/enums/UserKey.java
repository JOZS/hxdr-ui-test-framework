package com.hexagon.hxdr.testing.config.model.enums;

public enum UserKey {
    COMPANY_A_ADMIN,
    COMPANY_A_MAINTAINER,
    COMPANY_A_EMPLOYEE_1,
    COMPANY_A_EMPLOYEE_2,
    COMPANY_B_ADMIN,
    COMPANY_C_ADMIN,
    RESELLER_A_ADMIN,
    RESELLER_B_ADMIN,
    HXDR_ADMIN
}