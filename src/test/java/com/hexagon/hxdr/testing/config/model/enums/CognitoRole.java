package com.hexagon.hxdr.testing.config.model.enums;

public enum CognitoRole {
    ADMIN,
    USER
}
