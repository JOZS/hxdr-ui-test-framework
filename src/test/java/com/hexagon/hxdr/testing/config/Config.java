package com.hexagon.hxdr.testing.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ComponentScan({"com.hexagon.hxdr.testing"})
public class Config {

    @Bean
    @ConfigurationProperties(prefix = "environment")
    public EnvironmentProperties environmentProperties() {
        return new EnvironmentProperties();
    }

}
