package com.hexagon.hxdr.testing.config.model;

import com.hexagon.hxdr.testing.config.model.enums.UserKey;
import lombok.Data;

@Data
public class Account {
    private AccountAddress address;
    private boolean taxExempt;
    private double discountRate;
    private String sapAccountNumber;
    private double taxPercentage;
    private UserKey admin;
    private String accountPhone;
}
