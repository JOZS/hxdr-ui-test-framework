package com.hexagon.hxdr.testing.config.model;

import com.hexagon.hxdr.testing.config.model.enums.AccountKey;
import com.hexagon.hxdr.testing.config.model.enums.CognitoRole;
import lombok.Data;

import java.util.UUID;

@Data
public class TestUser {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String accountRole;
    private CognitoRole cognitoRole;
    private String description;
    private UUID userUUID;
    private String bearerToken;
    private AccountKey accountKey;
    private Account account;
}