package com.hexagon.hxdr.testing.config.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountAddress {
    private String street;
    private String streetNumber;
    private String postcode;
    private String city;
    private String state;
    private String country;
    private String additionalInfo;
}
